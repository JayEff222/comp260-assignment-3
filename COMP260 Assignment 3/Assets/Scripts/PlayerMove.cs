﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	//private SheepSpawn SheepSpawn;
	public float maxSpeed = 5.0f;
	public Vector2 direction;

	void Start () {
	//	SheepSpawn = FindObjectOfType<SheepSpawn> ();

	}

	// Update is called once per frame
	void Update () {
		
		direction.x = Input.GetAxis ("Horizontal");
		direction.y = Input.GetAxis ("Vertical");

		Vector2 velocity = direction * maxSpeed;

		transform.Translate(velocity * Time.deltaTime);
	}
}
