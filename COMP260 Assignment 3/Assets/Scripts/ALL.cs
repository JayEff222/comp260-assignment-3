﻿using UnityEngine;
using System.Collections;

public class ALL : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}
}

/*

using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {
	//public float speed = 4.0f;
	//public float turnSpeed = 180.0f;
	public Transform target; 
	public Transform target2;
	public Vector2 heading = Vector3.right;

	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	private float speed; 
	private float turnSpeed;

	public ParticleSystem explosionPrefab;

	// Use this for initialization
	void Start () {
		PlayerMove player = (PlayerMove)FindObjectOfType (typeof(PlayerMove));
		target = player.transform;

		PlayerMove2 player2 = (PlayerMove2)FindObjectOfType (typeof(PlayerMove2));
		target2 = player2.transform;

		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate (angle);

		speed = Mathf.Lerp (minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp (minTurnSpeed, maxTurnSpeed, Random.value);

	}

	void OnDestroy(){
		ParticleSystem explosion = Instantiate (explosionPrefab);
		explosion.transform.position = transform.position;

		Destroy (explosion.gameObject, explosion.duration);


	}
	
	// Update is called once per frame
	void Update () {
	

		Vector2 direction = target.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;


		//Vector2 Close;
		if (direction.magnitude > direction2.magnitude) {
			//Close = direction;
			float angle = turnSpeed * Time.deltaTime;
			//} else {
			//	Close = direction2; 
			//}
			//float angle = turnSpeed * Time.deltaTime;


			if (direction2.IsOnLeft (heading)) {
				heading = heading.Rotate (angle);
			} else {
				heading = heading.Rotate (-angle);
			}
			//Vector2 velocity = direction * speed;


			transform.Translate (heading * speed * Time.deltaTime);
			//transform.Translate(velocity * Time.deltaTime);
		} else {
			float angle = turnSpeed * Time.deltaTime;
			if (direction.IsOnLeft (heading)) {
				heading = heading.Rotate (angle);
			} else {
				heading = heading.Rotate (-angle);
			}
			transform.Translate (heading * speed * Time.deltaTime);
		}
	}
	void OnDrawGizmos(){
		Gizmos.color = Color.red;
		Gizmos.DrawRay (transform.position, heading);

		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;

		Gizmos.DrawRay (transform.position, direction);
		Gizmos.DrawRay (transform.position, direction2);

	}
}




using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {
	public Transform target;


	public BeeMove beePrefab;
	//public PlayerMove player;
	public int nBees = 50; 

	public float xMin, yMin;
	public float width, height;
	private float timeTillNext,TimeSinceLast = 0f;
	public float minBeeP, MaxBeeP;
	private int beeNum = 0;
	// Use this for initialization
	void Start () {
		createBee ();

	

}
public void DestroyBees(Vector2 center, float radius){
	for (int i = 0; i < transform.childCount; i++) {
		Transform child = transform.GetChild (i);
		Vector2 v = (Vector2)child.position - center;
		if (v.magnitude <= radius) {
			Destroy (child.gameObject);
		}

	}

}
void createBee(){
	BeeMove bee = Instantiate (beePrefab);

	bee.transform.parent = transform;
	bee.gameObject.name = "Bee " + beeNum;

	float x = xMin + Random.value * width;
	float y = yMin + Random.value * height;
	bee.transform.position = new Vector2 (x, y);

	timeTillNext =  Mathf.Lerp (minBeeP, MaxBeeP, Random.value);
	beeNum++;
	TimeSinceLast = 0f;
}
// Update is called once per frame
void Update () {
	TimeSinceLast += Time.deltaTime;
	if (TimeSinceLast >= timeTillNext) {
		createBee ();
	}
}
}




using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
	private BeeSpawner beeSpawner;
	public float maxSpeed = 5.0f;
	public Vector2 direction;
	public float destroyRadius = 1.0f;
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			beeSpawner.DestroyBees (transform.position, destroyRadius);
		}
		direction.x = Input.GetAxis ("Horizontal");
		direction.y = Input.GetAxis ("Vertical");

		Vector2 velocity = direction * maxSpeed;

		transform.Translate(velocity * Time.deltaTime);
	}
}


using UnityEngine;
using System.Collections;

public class PlayerMove2 : MonoBehaviour {
	//public Vector2 move;
	//public Vector2 volocity;
	private BeeSpawner beeSpawner;
	public float maxSpeed = 5.0f;
	public Vector2 direction;
	public float destroyRadius = 1.0f;
	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire2")) {
			beeSpawner.DestroyBees (transform.position, destroyRadius);
		}
		direction.x = Input.GetAxis ("Horizontal2");
		direction.y = Input.GetAxis ("Vertical2");

		Vector2 velocity = direction * maxSpeed;

		transform.Translate(velocity * Time.deltaTime);
	}
}











*/